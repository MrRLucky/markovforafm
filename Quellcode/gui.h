#ifndef _GUI_H_
#define _GUI_H_

#include <cv.h>
#include <highgui.h>
#include <string>
#include <opencv2/opencv.hpp>
#include "tracker.h"
#include "input.h"
#include <iostream>
#include <fstream>


class Gui
{
	private:

    char **argv;
    int firstTime;
    int argc;
	Tracker *p_tracker;
	Input *p_map;
	Input *p_tip;
	cv::Mat tipImg;
	int mapRowCount;
	int mapColCount;
	cv::Mat *p_probImg;
    int x_old;
    int y_old;
    float driftNmsX;
    float driftNmsY;
	void makeProbImage(Table* p_oldTable, cv::Point maxPoint);
	void showImage(cv::Point maxPoint,int x, int y, int waitKey, cv::Point2f driftTotal, cv::Point estimatedDrift,float scale);
	float computeDriftX(cv::Point movementVector);
	float computeDriftY(cv::Point movementVector);



	//used for the drift calculation
	float accumulatedDistanceX;
	float accumulatedDistanceY;

	std::string outputFilePath;

	public:
	void followPath();
	void onMouse( int event, int x, int y, int);
	Gui(Tracker*, Input*, Input*, char *argv[], int argc, std::string logifile);
	void randomMouse();
	void initWindows();

};
#endif
