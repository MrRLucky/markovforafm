#ifndef _INPUT_H_
#define _INPUT_H_

#include <cv.h>
#include <string>
#include <highgui.h>
#include <list>
class Input
{
	private:
	cv::Mat data;


	public:
	Input();

	Input(std::string);

	int getRowCount();

	int getColCount();

    std::string getFileName();

	void showImage(std::list<int>*,int realRow, int realCol);

	void setPixelValueAt(cv::Vec3b color);

	void readDataFromFile(std::string filename);

	cv::Mat getMat();

	inline cv::Vec3b getValueAt(int y, int x)
	{

		return data.ptr<cv::Vec3b>(y)[x];
	}

	int findValuesInMap(const cv::Vec3b searchValue, char* p_newPoints, int threshold);

};
#endif
