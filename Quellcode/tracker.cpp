    #include "tracker.h"
#define _USE_MATH_DEFINES
#define DELTA cv::Vec3b(0,0,0)



Tracker::Tracker(float scale ,Gauss* p_filter, Input* p_tip, Input* p_map, std::string logFilePath, int threshold)
{

    this->scale = scale;
	this->p_filter = p_filter;
	this->p_tip = p_tip;
	this->p_map = p_map;
	this->rowCount = p_map->getRowCount();
	this->colCount = p_map->getColCount();
    std::string logFileName(logFilePath);
    this->threshold = threshold;
    logFileName.append("logfile");
    p_file = fopen (logFileName.c_str(),"w");
    p_speedFile = fopen (logFileName.append("_speed").c_str(),"w");

    if(p_file == NULL) printf("logfile could not be opened");
    if(p_speedFile == NULL) printf("speedfile could not be opened");

    //for logging purposes (number of pipeline steps since program start)
    offset = 0;

    firstIteration = true;

	p_newPoints = new char[rowCount*colCount];

    p_newTable = new Table(rowCount, colCount);
    p_oldTable = new Table(rowCount, colCount);
    p_newTable->initTable(0.0);
	p_oldTable->initTable(1.0);


}

Tracker::~Tracker()
{
	delete p_oldTable;
	std::list<cv::Point2d*>::iterator it;
	delete p_newPoints;

    fclose(p_file);
    fclose(p_speedFile);
}
//Top level of localization algorithm.
//It calls findAndEnterValues that locates the newest input in the Overview map and Enters them into the new probabillity table
//Then a gauss filter is applied to reduce noise and inaccurate movement errors
//Then new and the old Probabillity tables are folded to compute current position
std::pair<Table*, cv::Point> Tracker::track(cv::Vec3b vec, cv::Point traveled)
{

		std::cout << "__________________________________________________________________________________" << std::endl;

        struct timeval time, begin, end;


        gettimeofday(&begin, NULL);
		//rset data structures

        for(int i = 0; i < rowCount*colCount; i++)
        {
            p_newPoints[i] = 0;
        }
		p_newTable->resetTable();

		gettimeofday(&end, NULL);
        fprintf(p_speedFile,"%lu ",(end.tv_sec - begin.tv_sec)*1000000L + end.tv_usec - begin.tv_usec);

        gettimeofday(&time, NULL);
        log("display_begin",time,"findValuesinMap");
        gettimeofday(&begin, NULL);


//Step 1-----------------------------Look for possible locations and enter them into probability matrix---------------------------------

        if(max > 0.5) threshold /= max;
        int newPointCount = p_map->findValuesInMap(vec, p_newPoints, threshold);

        gettimeofday(&end, NULL);

        fprintf(p_speedFile,"%lu ",(end.tv_sec - begin.tv_sec)*1000000L + end.tv_usec - begin.tv_usec);

        gettimeofday(&begin, NULL);

        float factor = 1.0/newPointCount;
        float noise = 1.0/newPointCount/100000;

		if(newPointCount != 0)
		{

             //enter newly found points into table
            float* p_array = p_newTable->getData();
			for(int i = 0; i < rowCount*colCount; i++)
			{
                   if(p_newPoints[i])
                   {
                        p_array[i] = factor;
                    }
                    else
                    {
                        p_array[i] = noise;
                    }
			}
        }
        else
        {
            std::cout << "color vector could not be found in map" << std::endl;
            exit(EXIT_FAILURE);
        }

        gettimeofday(&end, NULL);

        fprintf(p_speedFile,"%lu ",(end.tv_sec - begin.tv_sec)*1000000L + end.tv_usec - begin.tv_usec);

//Step 2-----------------------------Apply filter to probability matrix ------------------------------------------------------------------


        log("findValuesinMap",time,"gauss");

            //printf("filter full duration %f duration per iteration: %f \n",(float(end - start) / CLOCKS_PER_SEC), (float(end - start) / CLOCKS_PER_SEC)/iterations);

        log("gauss",time,"shift");

//Step 3-----------------------------Shift probability matrix ----------------------------------------------------------------------------





        gettimeofday(&begin, NULL);

        if(!firstIteration)
        {
            int shiftRow = traveled.y;
            int shiftCol = traveled.x;

            p_oldTable->shift(shiftRow, shiftCol, noise );
        }
        gettimeofday(&end, NULL);

        gettimeofday(&begin, NULL);


        p_filter->applyFilter(p_oldTable->getData(), p_speedFile);


        gettimeofday(&end, NULL);

        fprintf(p_speedFile,"%lu ",(end.tv_sec - begin.tv_sec)*1000000L + end.tv_usec - begin.tv_usec);


        fprintf(p_speedFile,"%lu ",(end.tv_sec - begin.tv_sec)*1000000L + end.tv_usec - begin.tv_usec);
        log("shift",time,"multiply");
        firstIteration = false;
        //Step 4-----------------------------Mutliply old and new probability matrices----------------------------------------------------------


        gettimeofday(&begin, NULL);
        //multiply old prob table and new prob table to find new maximum robabillity for current location
        p_oldTable->multiplyWith(p_newTable);

        gettimeofday(&end, NULL);

        fprintf(p_speedFile,"%lu ",(end.tv_sec - begin.tv_sec)*1000000L + end.tv_usec - begin.tv_usec);

        //normalize Table
        //normalize() returns false if p_oldTable could not be normalized (sum of all Entrys equals zero)

        gettimeofday(&begin, NULL);

        cv::Point maxPoint = p_oldTable->normalize_getMax();

        gettimeofday(&end, NULL);

        fprintf(p_speedFile,"%lu \n",(end.tv_sec - begin.tv_sec)*1000000L + end.tv_usec - begin.tv_usec);

        log("multiply",time,"display_end");

        std::pair<Table*, cv::Point> returnValue(p_oldTable, maxPoint);

        this->max = p_oldTable->getValueAt(maxPoint.x,maxPoint.y);

        return returnValue;
}


void  Tracker::log(std::string previous, timeval dataOrigin, std::string subsequent)
{
    struct timeval now,last;
    gettimeofday(&now, NULL);
    int64 time_now          = now.tv_sec        *1000000000ULL + now.tv_usec        *1000ULL;
    int64 time_dataOrigin   = dataOrigin.tv_sec *1000000000ULL + dataOrigin.tv_usec *1000ULL;
    fprintf(p_file,"%s %llu %llu %u %llu %ld %d %lu %s \n",previous.c_str(),time_dataOrigin,time_now, 0,++offset, 0,0,0,subsequent.c_str());



}







