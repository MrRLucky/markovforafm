#ifndef _GAUSS_H_
#define _GAUSS_H_
#include <stdio.h>
#include <math.h>
class Gauss
{
private:


    void generateGaussKernel(float*);
    float *a_d , *b_d, *kernel_h, *kernel_d;
    size_t nSize;
    size_t kernelSize;
    int N, colCount, rowCount, block_size, n_blocks;
	float* p_kernel;
	float* p_helpArray;
	float deviation;
	int gaussRange;
	int gaussHalf;
	void generateGaussKernel();


public:
	Gauss(int, int, int, float);
	~Gauss();
	int getRange();
	float* applyFilter(float* input, FILE* p_speedFile);
};
#endif
