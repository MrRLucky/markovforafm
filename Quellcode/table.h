#ifndef _TABLE_H_
#define _TABLE_H_


#include <string>
#include <list>
#include <iostream>
#include <cv.h>
class Table
{
	private:

	int rowCount;
	int colCount;
	float* p_data;

	public:

	Table(int, int);
	Table(int _rowCount, int _colCount, float* _p_data);
	~Table();

	float* getData();

	inline int getRowCount()
	{
		return rowCount;
	}
	inline int getColCount()
	{
		return colCount;
	}
	inline void setValueAt(int row, int col, float value)
	{
		p_data[row*colCount + col] = value;
	}

	inline void setValueAt1d(int pos1d, float value)
	{
		p_data[pos1d] = value;
	}

	//getValue at a certain position using 1-dimensional index
	inline float getValueAt1d(int pos1d)
	{
		return p_data[pos1d];
	}
	inline float getValueAt(int row, int col)
	{
		return p_data[row*colCount + col];
	}

	 //multiplays each position x,y from this table with the position x,y in newTable
    void multiplyWith(Table* newTable);

	//normalize values to 1
	cv::Point normalize_getMax();

	//shift table to fold probabilities
	void shift(int dx, int dy, float noise);

	void initTable(float value);
	void printTable(std::string heading);
	//returns the first found Max Point
	cv::Point getMax();
	//returns a list of all Points that share the Max Value
	std::list<cv::Point> getMaxList();
	void deleteTable();
	void resetTable();
};
#endif
