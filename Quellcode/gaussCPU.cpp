



#include "gauss.h"
#include <iostream>
#include <stdio.h>
#define _USE_MATH_DEFINES
Gauss::Gauss(int gaussRange, int rowCount, int colCount, float deviation)
{
    this->deviation = deviation;
	this->rowCount = rowCount;
	this->colCount = colCount;
	this->gaussRange = gaussRange;
	this->gaussHalf = gaussRange/2;

	p_helpArray =  new float[rowCount*colCount]();



	p_kernel = new float[gaussRange + 1];
	generateGaussKernel();
}
Gauss::~Gauss()
{
	delete[] p_kernel;
	delete p_helpArray;
}

//since the values read by the asm and the map are only similar a gaussian filter is used
//the 2 dimensional Gauss filter is split up into two one-dimensional operation hence reducing complexity
//furthermore the filter is only applied to the newly found possible locations and there surrounding (up to a distance of GAUSS_RANGE/2)
//These are stored in the p_newProbTable



float* Gauss::applyFilter(float* p_input, FILE* p_speedFile)
{
	//colwise
		for (int row = 0; row < rowCount ; ++row)
		{
			for (int col = 0; col < colCount; ++col)
			{
				float sum = 0;

				for(int d = -gaussHalf; d<= gaussHalf; d++)
				{

					int dCol = col + d;

					if(dCol >= 0 && dCol < colCount)
					{
						sum += p_input[row*colCount + dCol] * p_kernel[d + gaussHalf];
					}

				}
				p_helpArray[row*colCount + col] = sum;
			}
		}
	//rowwise
		for (int row = 0; row < rowCount ; ++row)
		{
			for (int col = 0; col < colCount; ++col)
			{
				float sum = 0;

				for(int d = -gaussHalf ; d <= gaussHalf ; d++)
				{
					int dRow = row + d;

					if(dRow >= 0 && dRow < rowCount)
					{
						sum += p_helpArray[dRow*colCount + col] * p_kernel[d + gaussHalf ];
					}

				}
				p_input[row*colCount + col] = sum;
			}

		}



	return p_input;
}






// generates the kernel used to for the Gauss Filter
void Gauss::generateGaussKernel()
{
	std::cout<< "Gauss Kernel " << std::endl;
	int idx = 0;
	for(int i = -(gaussHalf); i <= (gaussHalf); i++)
	{
		//normal distribution
		p_kernel[idx] = exp(-(i*i)/(2.0*this->deviation*this->deviation)) / (sqrt(M_PI)*M_SQRT2*this->deviation);
		std::cout<< p_kernel[idx] << " ";
		idx++;
	}
	std::cout << std::endl;

}

int Gauss::getRange()
{
	return this->gaussRange;
}

