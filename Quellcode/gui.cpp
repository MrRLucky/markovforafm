
#include "gui.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sstream>
//#include "boost/filesystem.hpp"

static void wrappedOnMouse(int event, int x, int y, int flags, void* ptr)
{
    Gui* p_gui = (Gui*)ptr;
    if(p_gui != 0L)
        p_gui->onMouse(event, x, y, flags);
}


Gui::Gui(Tracker *p_tracker, Input *p_map, Input *p_tip, char** argv, int argc, std::string outputFilePath)
{
    this->argc = argc;
    this->argv = argv;
	this->p_tracker = p_tracker;
	this->p_map = p_map;
	this->p_tip = p_tip;
	this->outputFilePath = outputFilePath;
	this->driftNmsX = atof(argv[7]);
    this->driftNmsY = atof(argv[8]);
	p_probImg = new cv::Mat(p_map->getRowCount(), p_map->getColCount(), CV_8U);
	tipImg = p_tip->getMat().clone();
	firstTime = true;
	srand(time(NULL));

	initWindows();

    x_old = 0;
	y_old = 0;

	cv::setMouseCallback( "tip", wrappedOnMouse, this );
}
void Gui::initWindows()
{
    cv::namedWindow("prob",0);
	cv::namedWindow("map",0);
	cv::namedWindow("tip",0);

	cvMoveWindow("prob", 0, 400);
	cvMoveWindow("map", 450, 0);
	cvMoveWindow("tip", 600, 400);

	cvResizeWindow("prob",500,500);
	cvResizeWindow("map",500,500);
	cvResizeWindow("tip",500,500);

	cv::imshow("prob", p_map->getMat());
	cv::imshow("map", p_map->getMat());
	cv::imshow("tip", p_tip->getMat());


}


void Gui::onMouse( int event, int x, int y, int flags)
{

   if((flags == cv::EVENT_LBUTTONDOWN ) && (flags == CV_EVENT_FLAG_SHIFTKEY))
    {
        exit(EXIT_SUCCESS);
    }

	if( event == cv::EVENT_LBUTTONDOWN )
	{
         cv::Point maxPoint;
        cv::Point traveled(x - x_old, y - y_old);

        traveled.x = traveled.x; 
        traveled.y = traveled.y; 

        std::pair<Table*, cv::Point> result = p_tracker->track(p_tip->getValueAt(y,x), traveled);

		Table* p_oldTable = result.first;

        x_old = x;
        y_old = y;

		maxPoint = result.second;

		std::cout << "Max Points: " << std::endl;

		makeProbImage(p_oldTable, maxPoint);

		showImage(maxPoint ,x, y,5, cv::Point(0,0), cv::Point(0,0),1);

	}
	else
	{
        if(event == cv::EVENT_RBUTTONDOWN)
        {
            followPath();
            system(("cd " + outputFilePath).c_str());
            system("gnuplot -e \"filename='plot.txt'\"");

        }

    }
}

void Gui::followPath()
{
             cv::Point maxPointMap;

            std::ifstream pathFile;
            pathFile.open("path.txt", std::ios::in);


            if(!pathFile.good())
            {
                std::cout << "error reading path file" << std::endl;
                exit(1);
            }

            std::list<cv::Point> points;

            cv::Point point;
            cv::Point prevPoint;
            int steps = 0;

            //read firstPoint

            pathFile >> prevPoint.x >> prevPoint.y;

            //at first Point to list

            points.push_back(prevPoint);

            //add next point and in-between points to list

            while(pathFile >> point.x >> point.y >> steps)
            {
                int deltax = (point.x - prevPoint.x)/steps;
                int deltay = (point.y - prevPoint.y)/steps;

                for(int i=1; i <= steps; i++)
                {
                    cv::Point newPoint(prevPoint.x + i*deltax, prevPoint.y + i*deltay);
                    points.push_back(newPoint);
                   
                }

                prevPoint = point;
            }

            //output streams
            std::ofstream pathLog;
            std::ofstream plot;

            //open output streams
            pathLog.open((outputFilePath + "driftEstimation.txt").c_str(), std::ios::out);
            plot.open((outputFilePath + "localizationResult.txt").c_str(), std::ios::out);

            if(!plot.good())
            {
                std::cout << "outputFile plot" << std::endl;
            }
            if(!pathLog.good())
            {
                std::cout << "outputFile Error: pathlog" << std::endl;
            }


            std::cout << outputFilePath << std::endl;


            std::pair<Table*, cv::Point> result;

            //Drfit since last Drift update
            cv::Point2f drift(0,0);
            //Drift since Begin of measurement
            cv::Point2f driftTotal(0,0);

            //waypoint iterator
            std::list<cv::Point>::iterator waypointIt;

            //last waypoint is needed to calculate movement (newWaypoint - lastWaypoint)
            std::list<cv::Point>::iterator lastWaypointIt = points.begin();


            float totalDistance;

            //Real location including Drfit
            cv::Point2f lastRealPointTip = *(points.begin());

             int displacementx = atof(argv[10]);
            int displacementy = atof(argv[11]);
             float scale = atof(argv[3]);

            //Location where the Tip would be if there was no drift
            //The drift can be estimated from the difference between this Point and the Point the localisation found
            //cv::Point locNoDriftMap(displacementx + (*points.begin()).x*scale, displacementy+(*points.begin()).y*scale);
            cv::Point locNoDriftTip((*points.begin()).x,(*points.begin()).y);




            int iterationCount = 0;
            //"walk" along Waypoints
            for(waypointIt = ++points.begin(); waypointIt != points.end(); waypointIt++)
            {

                //x and y movement since last point in tip-Pixel
                cv::Point2f movementVectorTip = *(waypointIt)-*(lastWaypointIt);

                //movementVector *= scale;

                std::cout << "mvector = " << movementVectorTip.x << " " << movementVectorTip.y << std::endl;
                std::cout << "scale = " << scale << std::endl;

                //length of movement vector, needed to calculate the time that has passed
                drift.x = computeDriftX(movementVectorTip);
                drift.y = computeDriftY(movementVectorTip);

                totalDistance += sqrt(pow(movementVectorTip.x,2) + pow(movementVectorTip.y,2));

                //when the new real location is calculated the last real location, the movement of the tip
                // and drift must be taken into account
                cv::Point2f newRealPointTip = lastRealPointTip + movementVectorTip + drift;


                //this stopps the Program when Drift causes the tip to move out of the sample
                if(newRealPointTip.x < 0 || newRealPointTip.x > p_tip->getColCount() || newRealPointTip.y < 0 || newRealPointTip.y > p_tip->getRowCount())
                {
                    std::cout << "RealPoint ouside Picture, stopping";
                    exit(0);
                }




                //localize tip using the measurement at current real location and the movement of the tip (without drift)
                result = p_tracker->track(p_tip->getValueAt((int)floor(newRealPointTip.y + 0.5), (int)floor(newRealPointTip.x+0.5)), movementVectorTip*scale);

                // Tip Scaled
                driftTotal += drift;

                lastRealPointTip = newRealPointTip;

                lastWaypointIt = waypointIt;

                //Show results
                Table* p_oldTable = result.first;

                maxPointMap = result.second;


                //location without drift in tip image)
                locNoDriftTip.x = locNoDriftTip.x + movementVectorTip.x;
                locNoDriftTip.y = locNoDriftTip.y + movementVectorTip.y;

                //location without drift in (map image)
                cv::Point locNoDriftMap;
               locNoDriftMap.x = displacementx + (locNoDriftTip.x*scale);
               locNoDriftMap.y = displacementy + (locNoDriftTip.y*scale);

                cv::Point estimatedDrift = (maxPointMap - locNoDriftMap);


                pathLog << estimatedDrift.x<< " " << estimatedDrift.y << " " <<  driftTotal.x*scale << " " << driftTotal.y*scale << std::endl;



                int errorx = maxPointMap.x - (displacementx+newRealPointTip.x*scale);
                int errory = maxPointMap.y - (displacementy+newRealPointTip.y*scale);

                plot << sqrt(pow(errorx,2) + pow(errory,2)) << " " << driftTotal.x*scale << " " << driftTotal.y*scale << " " << errorx << " " << errory << std::endl;

                makeProbImage(p_oldTable, maxPointMap);

                showImage(maxPointMap,newRealPointTip.x, newRealPointTip.y,1,driftTotal, estimatedDrift, scale);

                 std::cout << "itcount = " << iterationCount << std::endl;
                iterationCount++;

            }
            points.clear();
            pathFile.close();
            pathLog.close();
            plot.close();
        }

//Calculates the drift from the distance the tip has moved.

float Gui::computeDriftX(cv::Point movementVector)
{
    // Drift in x and y direction in nanometer/second

    float drift = 0;


    int scanspeed = atoi(argv[9]);



    accumulatedDistanceX += sqrt(pow(movementVector.x, 2) + pow(movementVector.y, 2));

    drift = (accumulatedDistanceX) * driftNmsX/scanspeed;

    if(abs(drift) >= 1)
    {
        accumulatedDistanceX = 0;

    }
    else
    {
        drift = 0;
    }

    return drift;


}

float Gui::computeDriftY(cv::Point movementVector)
{
    // Drift in x and y direction in nanometer/second

    float drift=0;

    //scanning speed in nanometer/second
    int scanspeed = atoi(argv[9]);



    accumulatedDistanceY += sqrt(pow(movementVector.x, 2) + pow(movementVector.y, 2));

    drift = (accumulatedDistanceY) * driftNmsY/scanspeed;

    if(abs(drift) >= 1)
    {
        accumulatedDistanceY = 0;

    }
    else
    {
        drift = 0;
    }
    return drift;


}


void Gui::makeProbImage(Table* p_oldTable, cv::Point maxPoint)
{


	int rowCount = p_oldTable->getRowCount();
	int colCount = p_oldTable->getColCount();

	double maxValue = p_oldTable->getValueAt(maxPoint.y,maxPoint.x);


    if(maxValue == 0)
    {
        std::cout << "MaxValue == 0 !!";
        //exit (EXIT_FAILURE);
    }
    else
    {
        std::cout << "max value = " << maxValue << std::endl;
    }



	double factor = 255/maxValue;

	for(int y = 0; y < rowCount; y++)
	{
		for(int x = 0; x < colCount; x++)
		{
			p_probImg->at<unsigned char>(y,x) = floor(p_oldTable->getValueAt(y,x)*factor + 0.5);

		}
	}

}

void Gui::showImage(cv::Point maxPoint,int x, int y, int waitKey, cv::Point2f driftTotal, cv::Point estimatedDrift, float scale)
{
    int displacementx = atof(argv[10]);
    int displacementy = atof(argv[11]);

	if(false)//firstTime%250 == 0)
	{
        tipImg = p_tip->getMat().clone();

    }
    firstTime++;


	cv::Vec3b v = p_tip->getValueAt(y,x);

	cv::Scalar colorForDrawing = cv::Scalar(255,0,v[0]);

    cv::circle(tipImg,cv::Point(x,y),3,colorForDrawing,1,8,0);


    cv::Mat mapImg = p_map->getMat().clone();

    cv::circle(mapImg,maxPoint,3,cv::Scalar(255,0,0),1,8,0);

    std::stringstream ss;

    ss.str("");

    ss << "Estimated Drift = " << estimatedDrift.x << " " << estimatedDrift.y;

    putText(mapImg, ss.str() , cv::Point(20,20), cv::FONT_HERSHEY_COMPLEX_SMALL , 0.8 , cvScalar(250,0,0), 1, 8, false );

    ss.str("");

    ss << "Drift Total = " << driftTotal.x*scale<< " " << driftTotal.y*scale;

    putText(mapImg, ss.str() , cv::Point(20,40), cv::FONT_HERSHEY_COMPLEX_SMALL , 0.8 , cvScalar(255,0,0), 1, 8, false );

    ss.str("");

    ss << "Distance = " << maxPoint.x- (displacementx + x*scale) << " " << maxPoint.y-(displacementy + y*scale);

    putText(mapImg, ss.str() , cv::Point(20,60), cv::FONT_HERSHEY_COMPLEX_SMALL , 0.8 , cvScalar(250,0,0), 1, 8, false );



	cv::imshow("tip", tipImg);
	cv::imshow("map", mapImg);
	cv::imshow("prob", *(p_probImg));

	cv::waitKey(waitKey);

}



