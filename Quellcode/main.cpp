

#include <stdio.h>
#include <string.h>
#include "input.h"
#include "gauss.h"
#include "tracker.h"
#include "gui.h"
#include <iostream>



std::string createNewDirectory(char *argv[])
{

            std::stringstream ss;
            ss.str("");
            ss << "ergebnisse/";
            ss << argv[1] << "_" << argv[2] << "/";
            ss << argv[3] << "_" << argv[4] << "_" << argv[5] << "/";
            ss << argv[6] << "_" << argv[7] << "_" << argv[8] << "_" << argv[9] << "_" << "/";




            std::string mkdir("mkdir -p ");

            system(mkdir.append(ss.str()).c_str());
            std::cout << "created folder: " << ss.str() << std::endl;

            return ss.str();
}

//#define gaussRange 10

//arguments:
//tip image
//map image
//scale map to tip
//threshold for comparison of colorvectors
//gaussradius
//gauss deviation
//drift nm/s x-axis
//drift nm/s y-axis
//scanspeed nm/s
//map pixel per NM
//offset x
//offset y
int main(int argc, char **argv)
{


    char *tip = argv[1];
    char *map = argv[2];
    float  scale = atof(argv[3]);
    int threshold = atoi(argv[4]);
    int gaussRadius = atoi(argv[5]);
    float deviation = atof(argv[6]);



    Input* 		p_tip = new Input(tip);
	Input* 		p_map = new Input(map);

    std::string outputFilePath = createNewDirectory(argv);

    int colCount = p_map->getColCount();
    int rowCount = p_map->getRowCount();

	Gauss*		p_filter = new Gauss(gaussRadius, colCount , rowCount, deviation);

	Tracker* 	p_tracker = new Tracker(scale, p_filter, p_tip, p_map, outputFilePath, threshold);

	Gui* 		p_gui = new Gui(p_tracker,p_map,p_tip, argv, argc, outputFilePath);

	std::cout << "gaussRadius = " << gaussRadius << std::endl;

    cv::waitKey(0);
	delete p_map;
	delete p_tip;
	delete p_filter;
	delete p_tracker;
    delete p_gui;
	return 0;
}

