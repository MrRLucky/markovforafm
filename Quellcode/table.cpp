#include "table.h"
#include <cstring>



Table::Table(int rowCount, int colCount)
{
	this->rowCount = rowCount;
	this->colCount = colCount;
	p_data = new float[rowCount*colCount]();
}

Table::Table(int rowCount, int colCount, float* p_data)
{
	this->rowCount = rowCount;
	this->colCount = colCount;
	p_data = new float[rowCount*colCount]();

	for (int i = 0; i < rowCount*colCount; ++i)
	{
        p_data[i] = p_data[i];
    }
}

Table::~Table()
{
	delete[] p_data;
}
//initiates tables once the size is known
void Table::initTable(float value)
{
	for (int i = 0; i < rowCount*colCount; ++i)
	{
		p_data[i] = value;
	}
}


float* Table::getData()
{
	return p_data;
}

cv::Point Table::getMax()
{
    cv::Point maxPoint(0,0);
    float max = 0;
    float value = 0;
    for (int y = 0; y < rowCount; ++y)
    {
        for (int x = 0; x < colCount; ++x)
        {
            value = getValueAt(y,x);
            if(value > max)
            {
                max = value;
                maxPoint.x = x;
                maxPoint.y = y;

            }
        }
    }
    return maxPoint;
}

std::list<cv::Point> Table::getMaxList()
{
	std::list<cv::Point>list;
	std::list<cv::Point>::iterator it;
	float max = 0;
	float value = 0;
	for (int y = 0; y < rowCount; ++y)
	{
		for (int x = 0; x < colCount; ++x)
		{
			value = getValueAt(y,x);
			if(value == max)
			{
				list.push_back(cv::Point (x,y));
			}
			else
			{
				if(value > max)
				{
					list.clear();

					list.push_back(cv::Point(x,y));
					max = value;
				}
			}

		}
	}
	return list;
}


//prints a tables to shell for error checking with very small tables (>11*11 cells)
void Table::printTable(std::string heading)
{
	std::cout << std::endl << heading << std::endl;

	int width = 10;


	std::cout << "    ";

	for(int i = 0; i < colCount; i++)
	{
		std::cout.width ( width );
		std::cout << i << " ";

	}
	std::cout << std::endl;
	std::cout << "   ";
	for(int i = 0; i < colCount*(width+1); i++)
	{

		std::cout <<  "-";

	}

	for(int i = 0; i < rowCount*colCount; i++)
	{
		if(i%rowCount == 0)
		{
			std::cout.width ( 2 );
			std::cout << std::endl << (i/rowCount) << ": ";
		}
		std::cout.setf ( std::ios_base::right, std::ios_base::basefield );
		std::cout.width ( width );
		std::cout.precision ( 5 ) ;
		std::cout << p_data[i]  << " ";
	}
	std::cout << std::endl << std::endl;
}


void Table::multiplyWith(Table* newTable)
{
    for(int i = 0; i < colCount*rowCount; ++i)
    {
            setValueAt1d(i, getValueAt1d(i) * newTable->getValueAt1d(i));
    }
}


void Table::resetTable()
{
	for(int i = 0; i < rowCount*colCount; i++)
	{
		p_data[i] = 0;
	}
}
//normalize Table so all probs add up to 1
//returns zero if sum equals 0
cv::Point Table::normalize_getMax()
{
	float sum = 0;
    cv::Point maxPoint(0,0);
    float max = 0;
    int maxIdx = 0;


	for(int i = 0; i < rowCount*colCount; i++)
	{
        float value = p_data[i];
        sum += value;

        if(value > max)
        {
            max = value;
            maxIdx = i;
        }

	}

	maxPoint.x = maxIdx % colCount;
    maxPoint.y = maxIdx / colCount;


	if(sum !=0)
	{
        for(int i = 0; i < rowCount*colCount; i++)
        {
            p_data[i] = p_data[i]/sum;
        }
    }
    else
    {
        std::cout << "p_oldTable contains only zeros, lokalisation failed, exiting";
        exit(EXIT_FAILURE);
    }

	return maxPoint;
}
//shifts a matrix by row and col.

//cols must be < colCount
//cols must be > -colCount
//rows must be < rowCount
//rows must be > -rowCount
//positiv row -> shift down
//negativ row -> shift up
//positiv col -> shift right
//negativ col -> shift left
void Table::shift(int rows, int cols, float noise)
{

	//shift right

	//for each row the entries are copied from left to right beginning from the last element
	//so now buffer is needed
	if(cols > 0)
	{
		for (int y = 0; y < rowCount; ++y)
		{
			for (int x = colCount-1; x >= cols; --x)
			{
				p_data[y * colCount + x] = p_data[y * colCount + x - cols];
			}
			for (int x = 0; x < cols; ++x)
			{
				p_data[y * colCount + x] = noise;
			}
		}
	}
	//shift left
	//for each row the entries are copied from right to left
	//cols < 0!
	else
	{
		for (int i = 0; i < rowCount; ++i)
		{
			for (int j = 0; j < colCount+cols; ++j) // cols < 0 !!!
			{
				p_data[i * colCount + j] = p_data[i * colCount + j - cols]; // cols < 0 !!!
			}
			for (int j = colCount + cols ; j < colCount ; ++j) // cols < 0 !!!
			{
				p_data[i * colCount + j] = noise;
			}
		}
	}

	//down
	if(rows > 0)
	{
		for (int i = 0; i < colCount; ++i)
		{
			for (int j = rowCount-1; j >= rows; --j)
			{
				p_data[j * colCount + i] = p_data[(j - rows) * colCount + i];
			}
			for (int j = 0 ; j < rows ; ++j)
			{
				p_data[j * colCount + i] = noise;
			}
		}

	}
	//up
	//rows  < 0!
	else
	{
		for (int i = 0; i < colCount; ++i)
		{
			for (int j = 0; j < rowCount+rows; ++j)
			{
				p_data[j * colCount + i] = p_data[(j - rows) * colCount + i];
			}
			for (int j = rowCount + rows ; j < rowCount ; ++j)
			{
				p_data[j * colCount + i] = noise;
			}
		}
	}
}

