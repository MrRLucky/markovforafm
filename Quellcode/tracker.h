
#ifndef _TRACKER_H_
#define _TRACKER_H_
#include "cv.h"
#include "input.h"
#include <string>
#include "table.h"
#include "gauss.h"
#include <iostream>
#include <stdlib.h>
#include <sys/time.h>
class Tracker
{



	private:
	//Point from previous iteration (used to calculate shift)
	bool firstIteration;
    float max;
	int rowCount;
	int colCount;
    float scale;
	Input* p_tip;
	Input* p_map;
	FILE * p_file;
	FILE* p_speedFile;
	Table* p_oldTable;
	Table* p_newTable;
    Table* p_backupTable;
	char* p_newPoints;
	int threshold;
    //for loging pprposes
    long unsigned int offset;
	Gauss* p_filter;

	void log(std::string previous, timeval dataOrigin, std::string subsequent);

	public:


	~Tracker();
	Tracker(float scale, Gauss* p_filter, Input* p_tip, Input* p_map, std::string logFilename, int theshold);

	std::pair<Table*, cv::Point> track(cv::Vec3b vec, cv::Point traveled);
};
#endif
