#include "input.h"
#include <iostream>
#include <stdlib.h>

Input::Input(){}

Input::Input(std::string filename)
{
	readDataFromFile(filename);

}



void Input::readDataFromFile(std::string filename)
{

	data = cv::imread(filename,CV_LOAD_IMAGE_COLOR);
	if(! data.data)
	{
		std::cout << "couldn't read image: " << filename << std::endl;
        exit(0);
	}
	else
	{
		std::cout << "found " << filename << std::endl;
	}
}

int Input::getRowCount()
{
	return data.rows;
}

int Input::getColCount()
{
	return data.cols;
}

cv::Mat Input::getMat()
{
	return data;
}

//looks for current value read by asm and looks for similar values in the map
//once found the location of those points is entered into the table of probable locations
int Input::findValuesInMap(const cv::Vec3b searchValue, char* p_newPoints, int threshold)
{
	int pointCount = 0;
	cv::Vec3b* row;
	size_t step = data.step1();

    for(int y = 0; y < data.rows ; y++)
        for(int x=0; x < data.cols ; x++)
        {
            row = data.ptr<cv::Vec3b>(y);
            cv::Vec3b mapValue = row[x];

            if((abs(searchValue[0]-mapValue[0]) <= threshold) && (abs(searchValue[1]-mapValue[1]) <= threshold) && (abs(searchValue[2]-mapValue[2]) <= threshold))
            {
                p_newPoints[y * data.cols + x] = 1;
                pointCount++;
            }
        }
	return pointCount;
}


