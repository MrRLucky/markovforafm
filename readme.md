﻿Readme zur Bachelorarbeit von Robin Luckey



-----------------------Kompilieren des Quellcodes:---------------------------

Um das Programm kompilieren zu können, muss OpenCV installiert sein. In der Arbeit wird Version 2.4.2 verwendet.

Damit die GPU beschleunigte Version kompiliert werden kann, muss außerdem CUDA installiert sein. In der Arbeit wurde Version 5.0 verwendet.

Einige Programmteile nutzen Linux-spezifische Befehle, weswegen das Programm unter Linux ausgeführt werden muss.

In dem Ordner "Quellcode" befinden sich alle Source-Dateien.
Je nachdem welche Version kompiliert werden soll, muss die Datei "gaussCPU.cpp" oder die "gaussCUDA.cu" eingebunden werden.

Befinden sich alle Quelldateien in einem Ordner (inklusive der entsprechenden gauss-Datei), kann z.B. wie folgt kompiliert werden:

CUDA:

```
#!bash
nvcc -O3 -Xcompiler "-w" -I "/usr/local/cuda-5.0/include" -L "/usr/local/cuda-5.0/lib64" `pkg-config opencv --cflags` *.cpp gaussGPU.cu -lcudart  `pkg-config opencv --libs`

```



CPU:
```
#!bash
g++ -o3 `pkg-config opencv --cflags` *.cpp  `pkg-config opencv --libs`

```

-----------------------Ausführen des Programms:------------------------------

Ein Programmaufruf sieht wie folgt aus:


./a.out Messspitze.bmp Karte.bmp Skalierung Threshold Gaußradius Standardabw. Drift-Geschw-X Drift-Geschw-y Scangeschw. Offset-x Offset-y

Für die drei Paare:

Paar 1:

./a.out Paar_1_Tip.bmp Paar_1_Karte.bmp 1 20 2 0.4 0.1 0.1 30 22 6

Paar 2:

./a.out Paar_2_Tip.bmp Paar_2_Karte.bmp 0.75 20 2 0.4 0.1 0.1 30 37 70

Paar 3:

./a.out Paar_3_Tip.bmp Paar_3_Karte.bmp 0.266 20 2 0.4 0.1 0.1 30 -10 122

Damit diese Aufrufe funktionieren, müssen das kompillierte Programm und die Bilder im selben Verzeichnis sein.
Außerdem muss eine Datei mit Namen "path.txt" im selben Verzeichnis liegen.

In "path.txt" ist der Wegverlauf gespeichert.

Der Wegverlauf muss wie folgt in der Path Datei angegeben werden:

Zeile 1: Startpunkt
Zeile 2: erster_Wegpunkt Anzahl_Zwischenstation
Zeile 3: zweiter_Wegpunkt Anzahl_Zwischenstation
...

An jeder Zwischenstation wird ein Update durchgeführt.

Beispiel:

Zwei Quadrat-Umrundungen mit 90 Iteration und einer Update-Rate von 10 Pixeln pro Update

100 100
200 100 10
200 200 10
100 200 10
100 100 10
100 100 10
200 100 10
200 200 10
100 200 10
100 100 10



---------------------Im Programm-----------------------------------------

Ist das Programm gestartet, sind drei Fenster zu sehen.

Wird im "tip"-Fenster mit der rechten Maustaste geklickt, dann wird der Pfad in der Path.txt-Datei abgefahren. 

Im "map"-Fenster wird dann die geschätze Drift, die simulierte Drift und der Abstand zwischen errechneter und echter Position angezeigt.

Wird alternativ mit der linken Maustaste geklickt, kann die Messspitzenposition direkt festgelegt werden.

Das Verhalten bei Vermischung von Links- und Rechtsklicks ist nicht definiert.

Wenn nicht gerade ein Pfad abgefahren wird, kann das Programm mit Druck auf eine beliebige Taste beendet werden.



--------------------Ergebnisse----------------------------------------

Für die Ergebnisse wird eine Dateistruktur angelegt, die von den Parametern abhängt:

Pfadsyntax:

Ausführungspfad/Ergebnisse/Messspitzenbild.bmp_Karte.bmp/Skalierung_Threshold_Filterradius/StdAbw_driftx_drifty_Scangeschw./

In diesem Verzeichnis werden vier Dateien angelegt:

"driftEstimation.txt" enthält (jeweils durch Leerzeichen getrennt):
geschätzteDriftInX, geschätzteDriftInY, simulierteDriftX, simulierteDriftY

"localizationResult.txt" enthält (jeweils durch Leerzeichen getrennt):
vektorAbstandSchätzungZurRealenPosition, simulierteDriftX, simulierteDriftY, abstandSchätzungZurRealenPositionX, abstandSchätzungZurRealenPositionY

"logfile_speed" enthält die Dauer der Einzelschritte jeder Iteration (jeweils durch Leerzeichen getrennt):
Initialisierung, Punkte_finden, Punkte_eintragen, Gaußfilter_berechnen, Matrixverschiebung, Matrixmultiplikation, Normieren_Maximum_finden

In der CUDA-Version ist die Gaußfilterdauer noch in:
Übertragung_zur_GPU, Berechnung_Zeile, Berechnung_Spalte und Übertragung_zur_CPU unterteilt.

"logfile" enthält die selben Daten nur in einem anderen Format.

Ich wünsche viel Spaß!
Robin Luckey